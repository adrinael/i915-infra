#!/usr/bin/env python3
#
# piglit results.json visualizer over history and hosts
#
# Originally by Tomi Sarvela for Intel OTC lab
#
import os
import argparse

from multiprocessing import Process, Queue

from mako.lookup import TemplateLookup
from mako import exceptions

from visutils import *
from common import htmlname
from collections import namedtuple

def writehtml(q):
    while True:
        try: build,host,testname,test = q.get()
        except TypeError:
            q.put(None)
            return

        # =================================================================
        # Manipulate dmesg string to highlight the dmesg-warnings
        # =================================================================
        # Get the list of dmesg-warnings
        # Search for dmesg-warning in dmesg and segregate to the list
        # Template render will update the css class based on this segration
        # =================================================================
        Dmesg = namedtuple('Dmesg', 'style message')
        if "dmesg-warnings" in test.keys():
            dmesg_warnings = test["dmesg-warnings"].split("\n")[:-1]
        else:
            dmesg_warnings = []

        dmesgs = []
        for line in test["dmesg"].split("\n")[:-1]:
            if line in dmesg_warnings:
                dmesgs.append(Dmesg('dmesg-warnings', line))
            else:
                dmesgs.append(Dmesg('dmesg-normal', line))
        test["dmesg"] = dmesgs

        if 'hostname' in test:
            realhost = test['hostname']
        else:
            realhost = host
        path = os.path.join(args.output, build, realhost)

        try: os.makedirs(path)
        except FileExistsError: pass
        filepath = os.path.join(path, htmlname(testname))
        title = testname+" on "+host+"@"+build

        with open(filepath, 'w') as f:
            try:
                page = template.render(path=path, title=title, testname=testname, test=test)
                f.write(page)
            except Exception as e:
                print("Warning: Failed to render", build, host, testname)

# MAIN

if __name__ == "__main__":
    ap = argparse.ArgumentParser(description="Visualize set of piglit json results")
    ap.add_argument('-o','--output', type=str, default='', help="Output html path")
    ap.add_argument('--nopass', default=False, action='store_true', help="Don't create pass-result htmls")
    ap.add_argument('--noskip', default=False, action='store_true', help="Don't create skip-result htmls")
    ap.add_argument('files', metavar='files', nargs='+', type=str, help="Files to be visualized")

    args = ap.parse_args()

    try: jsons = readfiles(args.files) # arranged by [(build,host)]
    except (KeyboardInterrupt) as e:
        print("ERROR: Keyboard interrupt while reading files")
        exit(1)

    rootdir = os.path.dirname(os.path.abspath(__file__))
    mako = TemplateLookup([os.path.join(rootdir, "templates"), rootdir])

    try: template = mako.get_template('test-result.mako')
    except:
        print(exceptions.text_error_template().render())
        exit(1)

    processes=10
    ps=set()
    q=Queue()

    for i in range(0, processes):
        p=Process(target=writehtml, args=(q,))
        ps.add(p)
        p.start()

    for build, host in jsons:
        for testname in jsons[(build,host)]['tests'].keys():
            test=jsons[(build,host)]['tests'][testname]
            if 'result' not in test:
                continue
            res=test["result"]
            if res=='notrun':
                continue
            if res=='pass':
                if not args.nopass: q.put((build, host, testname, test))
            elif res=='skip':
                if not args.noskip: q.put((build, host, testname, test))
            else: q.put((build, host, testname, test))

    # Put empty message to queue to go around after the work ...
    q.put(None)

    # ... and wait for subprocesses to exit cleanly
    for p in ps:
        p.join()

    # Clean up the last None message
    q.get()

    exit(0)

